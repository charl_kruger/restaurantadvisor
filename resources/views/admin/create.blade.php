@extends('master')

@section('content')
    
	<div class="container">

	    <h1>Add Restaurant</h1>

	    <form method="POST" action="/admin">

      {{ csrf_field() }}

		    <div class="form-group">

		    	<label for="name">Restaurant Name or Address</label>

		    	<input type="text" class="form-control" id="pac-input-custom">

	    	</div>

			<div class="form-group">
		    	<div id="map" style="height: 350px"></div>
	    	</div>

		    {{-- populated by google place id finder --}}
		    <input type="hidden" name="placeId" id="placeId" value="{{ old('placeId') }}">
        <input type="hidden" name="name" id="name" value="{{ old('name') }}">
        <input type="hidden" name="lat" id="lat" value="{{ old('lat') }}">
        <input type="hidden" name="lng" id="lng" value="{{ old('lng') }}">
        <input type="hidden" name="address" id="address" value="{{ old('address') }}">
        <input type="hidden" name="phone" id="phone" value="{{ old('phone') }}">

        {{-- Form errors --}}
        @if (count($errors) > 0)

          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
              <li>
                {{ $error }}
              </li>
              @endforeach
            </ul>
          </div>

        @endif

        {{-- Success message --}}
        @if (Session()->has('flash_message'))

          <div class="alert alert-success">
            
            {{ Session::get('flash_message') }}

          </div>

        @endif


	    	<div class="form-group">

	    		<button type="submit" class="btn btn-primary btn-lg btn-block">Save</button>

	    	</div>

	    </form>

    </div>



<script>

function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13
  });

  var input = document.getElementById('pac-input-custom');

  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow();
  var marker = new google.maps.Marker({
    map: map
  });
  marker.addListener('click', function() {
    infowindow.open(map, marker);
  });

  autocomplete.addListener('place_changed', function() {
    infowindow.close();
    var place = autocomplete.getPlace();
    if (!place.geometry) {
      return;
    }

    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);
    }

    // Set the position of the marker using the place ID and location.
    marker.setPlace({
      placeId: place.place_id,
      location: place.geometry.location
    });
    marker.setVisible(true);

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
        'Place ID: ' + place.place_id + '<br>' +
        place.formatted_address);
    infowindow.open(map, marker);

    // set the place id after the auto complete
	var setPlaceId = document.getElementById("placeId");
  var setName = document.getElementById("name");
  var setPlaceLat = document.getElementById("lat");
  var setPlaceLng = document.getElementById("lng");
  var setAddress = document.getElementById("address");
  var setPhone = document.getElementById("phone");
	
  setPlaceId.value = place.place_id;
  setName.value = place.name;
  setPlaceLat.value = place.geometry.location.lat();
  setPlaceLng.value = place.geometry.location.lng();
  setAddress.value = place.formatted_address;
  setPhone.value = place.formatted_phone_number;

  console.log(place);

  });
}

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiWj_7hmPshbclVb985iEV3kI9aLZ3M9U&libraries=places&signed_in=true&callback=initMap&places=restaurant"
        async defer></script>


@stop