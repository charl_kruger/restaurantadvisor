<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    
	protected $fillable = [
		'placeId',
		'name',
		'lat',
		'lng',
		'address',
		'phone'
	];
    
}
