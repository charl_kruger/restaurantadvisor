## Restaurant Advisor
Utelizes Google Places Finder to locate restaurants and save the 'place' to the db, once saved, places are available (json) via `../admin` for use on the front end

## Run the following from within the dir to get started
- `bower install`
- `npm install`
- `grunt`
- `php artisan migrate`
- `php artisan serve`

- navigate to the root to view the google maps listings.
- navigate to `admin/create` to list a new restaurant.