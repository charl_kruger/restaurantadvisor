//Gruntfile
module.exports = function(grunt) {
    'use strict';

    //Initializing the configuration object
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        // Task configuration
        clean: {
            js: ['public/js/app.js'],
            css: ['public/css/styles.css']
        },
        less: {
            dev: {
                options: {
                    compress: false,
                    strictMath: true,
                    sourceMap: true,
                    outputSourceFiles: true,
                    sourceMapURL: 'styles.css.map',
                    sourceMapFilename: 'public/css/styles.css.map'
                },
                files: {
                    "./public/css/styles.css":"./resources/assets/less/styles.less"
                }
            }
        },
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'public/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                }]
            }
        },
        concat: {
            options: {
                separator: ';',
                removeComments: true
            },
            dist: {
                src: [
                    'resources/assets/vendors/jquery/dist/jquery.js',
                    'resources/assets/vendors/bootstrap/dist/js/bootstrap.js',
                    'resources/assets/vendors/angular/angular.js',
                    'resources/assets/vendors/lodash/lodash.js',
                    'resources/assets/vendors/angular-route/angular-route.js',
                    'resources/assets/vendors/angular-bootstrap/ui-bootstrap.js',
                    'resources/assets/vendors/angular-simple-logger/dist/angular-simple-logger.js',
                    'resources/assets/vendors/angular-google-maps/dist/angular-google-maps.js',
                    'resources/assets/js/app.js',
                    'resources/assets/js/controllers.js'
                ],
                dest: 'public/js/app.js'
            }
        },
        uglify: {
            dist: {
                files: {
                    'public/js/app.min.js': ['public/js/app.js']
                }
            }
        },
        watch: {
            less: {
                files: ['resources/assets/less/**/*.less'],
                tasks: ['less'],
                options: {
                    livereload: true
                }
            },
            js: {
                files: ['./resources/assets/js/**/*.js'],
                options: {
                    livereload: true
                }
            },
        },
        notify_hooks: {
            options: {
                enabled: true,
                max_jshint_notifications: 5,
                title: "Restuarant Advisor",
                success: true,
                duration: 3
            }
        },

    });

    // Plugin loading
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-notify');

    // Task definition
    grunt.registerTask('default', ['clean', 'less', 'cssmin', 'concat', 'notify_hooks']);

};