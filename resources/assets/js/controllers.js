'use strict';

/* Controllers */

var restControllers = angular.module('restControllers', []);

restControllers.controller('HomeCtrl', ['$scope','uiGmapgoogle-maps',
    function($scope) {
        $scope.test = 'It Works!';
    }
]);


restControllers.controller('mainCtrl', function($scope, $http) {

$http.get("admin")
  .then(function (response) {
    $scope.restaurants = response.data;
    console.log($scope.restaurants);

    // var values = response.data;
    // var logg = [];
    // angular.forEach(values, function(value, key) {
    //   this.push(
    //       lat: ' + value.lat + ', ' +
    //       lng: ' + value.lng + '}'
    //     );
    // }, logg);
    // console.log(logg);

  });

    $scope.map = {
      center: {
        latitude: 40.1451,
        longitude: -99.6680
      },
      zoom: 4,
      bounds: {}
    };
    $scope.options = {
      scrollwheel: false
    };

var createRandomMarker = function(i, bounds, idKey) {
      var lat_min = bounds.southwest.latitude,
        lat_range = bounds.northeast.latitude - lat_min,
        lng_min = bounds.southwest.longitude,
        lng_range = bounds.northeast.longitude - lng_min;

      if (idKey == null) {
        idKey = "id";
      }

      var latitude = lat_min + (Math.random() * lat_range);
      var longitude = lng_min + (Math.random() * lng_range);
      var ret = {
        latitude: latitude,
        longitude: longitude,
        title: 'm' + i
      };
      ret[idKey] = i;
      return ret;
    };
    $scope.randomMarkers = [];
    // Get the bounds from the map once it's loaded
    $scope.$watch(function() {
      return $scope.map.bounds;
    }, function(nv, ov) {
      // Only need to regenerate once
      if (!ov.southwest && nv.southwest) {
        var markers = [];
        for (var i = 0; i < 50; i++) {
          markers.push(createRandomMarker(i, $scope.map.bounds))
        }
        $scope.randomMarkers = markers;
      }
    }, true);

    // $scope.randomMarkers = [{id: 0, latitude: "-26.092612", longitude: "28.055844999999977"}];


  });