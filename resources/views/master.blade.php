@include('includes.header')
@include('includes.nav')
<body>

	@yield('content')

	@include('includes.footer')

</body>
</html>