@extends('master')

@section('content')
    
	<!--Partials will be injected here-->
    <!-- <div ng-view></div> -->
    <h1>Test</h1>

    <div id="map_canvas" ng-controller="mainCtrl">
	    <ui-gmap-google-map center="map.center" zoom="map.zoom" draggable="true" options="options" bounds="map.bounds">
	        <ui-gmap-markers models="randomMarkers" coords="'self'" icon="'icon'">
	        </ui-gmap-markers>
	    </ui-gmap-google-map>
	</div>

@stop